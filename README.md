[![pipeline status](https://gitlab.com/hollyyfc/hollycui_docker/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_docker/-/commits/main)

# 🐳 Holly's Rust Actix Web with Docker

## Description

This project is a web-based "**Rock-Paper-Scissors**" game, developed in [Rust](https://www.rust-lang.org/) with the [Actix web](https://actix.rs/) framework. It features a user-friendly interface for playing against the computer, with game outcomes dynamically generated and displayed. Containerized with [Docker](https://www.docker.com/products/docker-desktop/) for easy deployment, the game illustrates the effective use of Rust for web development and Docker for application distribution.

## Demo

- Web Game 

![app1](https://gitlab.com/hollyyfc/hollycui_docker/-/wikis/uploads/a38f7a7a0a095f1a649ca27eb8ff6987/app1.png)

![app2](https://gitlab.com/hollyyfc/hollycui_docker/-/wikis/uploads/0c266f2621e9218c6279bb0a2e2a0276/app2.png)

- Docker Image and Container 

![dk-image](https://gitlab.com/hollyyfc/hollycui_docker/-/wikis/uploads/3f9123199163b1c2ddb526895d9b277a/dk-image.png)

![dk-container](https://gitlab.com/hollyyfc/hollycui_docker/-/wikis/uploads/8393dc1bba7d7dc4ce21c02f7e6dc36e/dk-container.png)

## Steps Walkthrough

- **Build Cargo Project** 
    - `cargo new <YOUR-PROJECT-NAME>` in desired directory
    - Add `actix-web`, `actix-files`, and relative dependencies in `Cargo.toml`
    - Build Rust functions in `src/main.rs` implementing Actix structures 
    - Add customized HTML files (`index.html`) for webpage UI
    - `cargo run`
        - Ensure *Xcode* and *Command Line Tools* are installed or up to date!
        - `conda deactivate` if *Anaconda* is installed to environment before 💢
- **Build Dockerfile**
    - Ensure *Docker Desktop* is installed or up to date
    - Create a `Dockerfile` in project root resolving Docker image and container creation 
    - `docker build -t <YOUR-IMAGE-NAME> .` while *Docker Desktop* is open
    - `docker run -d -p 8080:8080 <YOUR-IMAGE-NAME>` for running the app on `localhost:8080`
    - `docker ps` for inspecting the running containers 
    - `docker stop <CONTAINER-ID>` for stopping a container with `CONTAINER-ID` retrieved from last step
    - `docker rm <CONTAINER-ID>` for removing the container from using up undesired resources 
