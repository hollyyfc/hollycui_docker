# Build stage
FROM rust:1-slim-bookworm AS builder

# Create a new empty shell project
RUN USER=root cargo new --bin rock_paper_scissors
WORKDIR /rock_paper_scissors

# Cache dependencies
# Note: The 'cargo init' is not necessary if the project is already a cargo project
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src

# Compile application
RUN rm ./target/release/deps/rock_paper_scissors*
RUN cargo build --release

# Run stage
FROM bitnami/minideb:bookworm

# Copy compiled binary
COPY --from=builder /rock_paper_scissors/target/release/rock_paper_scissors .

# Set the environment to production
ENV ROCKET_ENV=production

# Expose the port your application listens on
EXPOSE 8080

# Run server
CMD ["./rock_paper_scissors"]
