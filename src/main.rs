use actix_files as fs;
use actix_web::{web, App, HttpServer, HttpResponse, Result};
use rand::seq::SliceRandom;
use std::sync::Mutex;

struct AppState {
    game_choice: Mutex<Vec<&'static str>>,
}

async fn play_game(query: web::Query<std::collections::HashMap<String, String>>, data: web::Data<AppState>) -> Result<HttpResponse> {
    let user_choice = query.get("choice").expect("No choice made!");
    let game_choices = data.game_choice.lock().unwrap();
    let server_choice = game_choices.choose(&mut rand::thread_rng()).unwrap();

    let outcome = match (*server_choice, user_choice.as_str()) {
        ("rock", "scissors") | ("scissors", "paper") | ("paper", "rock") => "You lose!",
        (c, u) if c == u => "It's a tie!",
        _ => "You win!",
    };

    let response_html = format!(
        "<h1>Your choice: {}</h1><h1>Server's choice: {}</h1><h2>{}</h2><a href='/'>Play again!</a>",
        user_choice, server_choice, outcome
    );

    Ok(HttpResponse::Ok().content_type("text/html").body(response_html))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_data = web::Data::new(AppState {
        game_choice: Mutex::new(vec!["rock", "paper", "scissors"]),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(app_data.clone())
            .service(web::resource("/").route(web::get().to(|| async { fs::NamedFile::open("index.html") })))
            .route("/play", web::get().to(play_game))
    })

    .bind("0.0.0.0:8080")?
    .run()
    .await
}



